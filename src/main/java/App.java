import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import java.io.IOException;

public class App {
    public static void main(String[] args) {
        try(CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet("https://speller.yandex.net/services/spellservice/checkText?text=мАсКвА");
            CloseableHttpResponse response1 = httpclient.execute(httpGet);
            System.out.println(response1.getStatusLine());
            String entity1 = EntityUtils.toString(response1.getEntity());
            System.out.println(entity1);
        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            HttpPost httpPost = new HttpPost("https://httpbin.org/post");
            httpPost.setHeader("User-Agent", "Java client");
            httpPost.setEntity(new StringEntity("Any data"));
            HttpResponse response2 = client.execute(httpPost);
            System.out.println(EntityUtils.toString(response2.getEntity()));
        } catch(ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        String sURL = "http://speller.yandex.net/services/spellservice";
        String sXML = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:spel=\"http://speller.yandex.net/services/spellservice\">\n" +
                "   <soapenv:Header/>\n" +
                "   <soapenv:Body>\n" +
                "      <spel:CheckTextRequest lang=\"en\" options=\"0\" format=\"\">\n" +
                "         <spel:text>beee</spel:text>\n" +
                "      </spel:CheckTextRequest>\n" +
                "   </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        try (CloseableHttpClient httpClient = HttpClients.createDefault()){
            HttpPost httpPost = new HttpPost(sURL);
            StringEntity strEnt = new StringEntity(sXML);
            httpPost.setEntity(strEnt);
            httpPost.addHeader("SOAPAction", "http://speller.yandex.net/services/spellservice/checkText");
            HttpResponse response3 = httpClient.execute(httpPost);
            System.out.println(response3.getStatusLine().getReasonPhrase());
            System.out.println(EntityUtils.toString (response3.getEntity()));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
